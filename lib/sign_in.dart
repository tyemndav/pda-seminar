import 'package:flutter/material.dart';
import 'package:tripper_and_trapper/home.dart';

class SignIn extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Basic AppBar'),
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.pop(context);
          }
        )
      ),

      body: Container(
        height: double.infinity,
        width: double.infinity,
        padding: EdgeInsets.all(20.0),
        child: ConstrainedBox(
          constraints: BoxConstraints.expand(),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.fromLTRB(0, 10, 0, 40),
                      child: TextField(
                        autofocus: true,
                        decoration: InputDecoration(
                            border: OutlineInputBorder(),
                            labelText: 'Email'
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.fromLTRB(0, 0, 0, 40),
                      child: TextField(
                        obscureText: true,
                        decoration: InputDecoration(
                            border: OutlineInputBorder(),
                            labelText: 'Password',
                            suffixIcon: Icon(Icons.remove_red_eye)
                        ),
                      ),
                    ),
                    RaisedButton(
                      color: Colors.deepPurple,
                      textColor: Colors.white,
                      child: const Text("SIGN IN"),
                      onPressed: () {
                        Navigator.of(context)
                            .push(MaterialPageRoute<Null>(builder: (BuildContext context) {
                          return new Home();
                        }));
                      },
                    ),
                    FlatButton(
                      color: Colors.deepPurple,
                      textColor: Colors.white,
                      child: const Text("FORGOT PASSWORD"),
                      onPressed: null,
                    ),
                  ],
                ),
              )
            ],
          )
        )
      )
    );
  }
}
