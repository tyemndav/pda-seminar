import 'package:flutter/material.dart';
import 'package:tripper_and_trapper/welcome.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Code Sample for material.AppBar.actions',
      theme: ThemeData(
        primarySwatch: Colors.deepPurple,
      ),
      home: Welcome(),
    );
  }
}
