import 'package:flutter/material.dart';
import 'package:tripper_and_trapper/sign_in.dart';

class Welcome extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: double.infinity,
        width: double.infinity,
        padding: EdgeInsets.all(20.0),
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/images/rectangle.png"),
            fit: BoxFit.cover,
          ),
        ),
        child: ConstrainedBox(
          constraints: const BoxConstraints.expand(),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              const Text(
                "Tripper and Trapper",
                style: TextStyle(
                    fontSize: 40.0, color: Colors.deepPurple
                ),
              ),
              Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    RaisedButton(
                      onPressed: () {
                        Navigator.of(context)
                            .push(MaterialPageRoute<Null>(builder: (BuildContext context) {
                          return new SignIn();
                        }));
                      },
                      color: Colors.deepPurple,
                      textColor: Colors.white,
                      child: const Text("SIGN IN"),
                    ),
                    RaisedButton(
                      onPressed: () {

                      },
                      color: Colors.deepPurple,
                      textColor: Colors.white,
                      child: const Text("REGISTER"),
                    ),
                  ],
                ),
              )
            ],
          )
        )
      )
    );
  }
}
